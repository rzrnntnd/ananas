# Установка исходных требований + перенос конфига

## Install requirements
```
ansible-galaxy install -r requirements.yml
```

### run mysql playbook
```
ansible-playbook -K -i inv.yml mysql.yml
```

### copy magento data from working dev to new server
```
rsync -avp --exclude 'var' magento@10.80.3.17:/var/www/magento/ /var/www/magento/
```

### create mysqldump on old dev server
```
mysqldump -u magento -pWYqVPnAE00ib --single-transaction --quick --lock-tables=false --no-tablespaces new_ga_base | gzip  > new_ga_base.sql.gz
mysqldump -u magento -pWYqVPnAE00ib --single-transaction --quick --lock-tables=false --no-tablespaces new_ga_quote | gzip  > new_ga_quote.sql.gz
mysqldump -u magento -pWYqVPnAE00ib --single-transaction --quick --lock-tables=false --no-tablespaces new_ga_sales | gzip  > new_ga_sales.sql.gz
```

### copy mysql dumps from remote server
```
scp root@165.22.19.77:~/new_ga_base.sql.gz ~/
scp root@165.22.19.77:~/new_ga_quote.sql.gz ~/
scp root@165.22.19.77:~/new_ga_sales.sql.gz ~/
```

### install pv and create script to gunzip mysql dumps
```
#!/bin/bash

echo "-ночало-"

echo "-ПЕРВЫЙ ПОШЁЛ-"
pv new_ga_base.sql.gz | gunzip | mysql -u magento -pWYqVPnAE00ib new_ga_base

echo "-ВТОРОЙ ПОШЁЛ-"
pv new_ga_quote.sql.gz | gunzip | mysql -u magento -pWYqVPnAE00ib new_ga_quote

echo "-ТРЕТИЙ ПОШЁЛ-"
pv new_ga_sales.sql.gz | gunzip | mysql -u magento -pWYqVPnAE00ib new_ga_sales

echo "-конiц-"
```

### Start playbook
```
ansible-playbook -K -i inv.yml ananas.yml
```
### install rdkafka with pecl
```
sudo apt install php-pear
sudo apt install php7.1-dev
sudo pecl install rdkafka
```
### Add the following line to your php.ini file:
```
extension=rdkafka.so
```

### create directory for shared folder
```
mkdir /mnt/magento_share
```

### mount shared directory
```
sudo mount -t nfs4 192.168.0.17:/data/gluster/gvol0 /mnt/magento_share
```

### create symlink for shared media
```
ln -s /mnt/magento_share/magento/shared/pub/media /var/www/magento/pub/media
```

### check below path, if there's no pictures, copy them like it shown below
```
cp -r /var/www/magento/app/design/frontend/Mygento/goldapple/web/images/labels/* /var/www/magento/pub/static/global/images/labels/
```

### in vue directory run
```
npm install
npm run build:client
```

### change url's in base

### magento reindex
```
indexer:reset
indexer:reindex catalog_product_flat catalog_category_product catalog_product_category
```